const std = @import("std");
const time = std.time;
const expect = std.testing.expect;
const Allocator = std.mem.Allocator;

const day_in_month = [12]u8{ 31, 28, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31 };

pub const Date = struct {
    day: u8,
    month: u8,
    year: u16,

    pub fn current() Date {
        const nineteen_seventy = Date{ .day = 1, .month = 1, .year = 1970 };
        const days_from_1970: u32 = @intCast(@divTrunc(time.timestamp(), 86400));
        return nineteen_seventy.add(days_from_1970);
    }

    pub fn add(x: Date, days: u32) Date {
        var result = x;

        var n = days;
        while (n > 0) {
            const left_in_month = monthLength(result.month, result.year) - result.day;

            if (left_in_month >= n) {
                result.day += @intCast(n);
                break;
            } else {
                result.day = 1;

                if (result.month == 12) {
                    result.year += 1;
                    result.month = 1;
                } else {
                    result.month += 1;
                }

                n -= left_in_month + 1;
            }
        }
        return result;
    }

    test add {
        const today = Date{ .day = 8, .month = 10, .year = 2023 };
        const new_year = Date{ .day = 1, .month = 1, .year = 2024 };

        const result = today.add(85);
        try expect(std.meta.eql(result, new_year));
    }

    // 2 bytes for dots.
    // 9 (2 + 2 + 5) bytes for day, month and year.
    pub const maxDateStringLen = 11;

    /// You need to free the slice to avoid a memory leak.
    /// Tested in Card's "To/From string" test.
    pub fn toString(self: Date, allocator: Allocator) ![]u8 {
        var result = std.ArrayList(u8).init(allocator);
        try result.ensureTotalCapacity(Date.maxDateStringLen);

        try std.fmt.format(
            result.writer(),
            "{}.{}.{}",
            .{ self.day, self.month, self.year },
        );

        return try result.toOwnedSlice();
    }

    /// Tested in Card's "To/From string" test.
    pub fn fromString(string: []const u8) !Date {
        const parseUnsigned = std.fmt.parseUnsigned;

        var date_iter = std.mem.splitSequence(u8, string, ".");

        return Date{
            .day = try parseUnsigned(u8, date_iter.next() orelse return error.@"Day not found", 10),
            .month = try parseUnsigned(u8, date_iter.next() orelse return error.@"Month not found", 10),
            .year = try parseUnsigned(u16, date_iter.next() orelse return error.@"Year not found", 10),
        };
    }
};

pub fn monthLength(month: u8, year: u16) u8 {
    if (month == 2 and LeapYear(year)) return 29 else return day_in_month[month - 1];
}

pub fn LeapYear(year: u16) bool {
    return ((year % 4 == 0) and (year % 100 != 0)) or (year % 400 == 0);
}

test LeapYear {
    try expect(!LeapYear(1700));
    try expect(!LeapYear(1894));
    try expect(!LeapYear(1900));
    try expect(!LeapYear(2001));

    try expect(LeapYear(1600));
    try expect(LeapYear(1876));
    try expect(LeapYear(1884));
    try expect(LeapYear(2020));
}
