const std = @import("std");
const fs = std.fs;
const Allocator = std.mem.Allocator;

const Date = @import("date.zig").Date;
const Card = @import("card.zig").Card;
const Result = @import("card.zig").Result;

pub const Deck = struct {
    dir: []const u8,
    iter: ?CardIterator,
    allocator: Allocator,

    pub fn load(dir: []const u8, allocator: Allocator) !Deck {
        return Deck{
            .dir = dir,
            .iter = try CardIterator.new(dir, allocator),
            .allocator = allocator,
        };
    }

    pub fn due(self: *Deck) !?*Card {
        if (self.iter == null) return null;
        const current = Date.current();

        while (try self.iter.?.next()) |card| {
            if (card.due.year != current.year and card.due.year < current.year) return card;
            if (card.due.month != current.month and card.due.month < current.month) return card;
            if (card.due.day <= current.day) return card;
        }

        self.iter = null;
        return null;
    }

    pub fn deinit(self: *Deck) !void {
        if (self.iter != null) try self.iter.?.deinit();
        self.* = undefined;
    }
};

pub const CardIterator = struct {
    dir: []const u8,
    iter: fs.Dir.Iterator,
    /// Storing the buffer to correctly deallocate memory that is used in the card.
    prev: ?struct { card: Card, path: []const u8 },
    allocator: Allocator,

    pub fn new(path: []const u8, allocator: Allocator) !CardIterator {
        const dir = try fs.cwd().openDir(path, .{ .iterate = true });

        return CardIterator{
            .dir = path,
            .iter = dir.iterate(),
            .prev = null,
            .allocator = allocator,
        };
    }

    /// The reference to the previous card will become invalid.
    pub fn next(self: *CardIterator) !?*Card {
        try self.deinit();

        var entry: fs.Dir.Entry = undefined;
        while (true) {
            entry = try self.iter.next() orelse return null;
            if (entry.kind == .file) break;
        }

        const path = try fs.path.join(self.allocator, &[_][]const u8{ self.dir, entry.name });

        const file = try fs.cwd().openFile(path, .{ .mode = .read_only });
        defer file.close();

        var buffer_list = std.ArrayList(u8).init(self.allocator);
        try file.reader().readAllArrayList(&buffer_list, std.math.maxInt(usize));
        const buffer = try buffer_list.toOwnedSlice();
        defer self.allocator.free(buffer);

        self.prev = .{
            .card = try Card.fromString(buffer, self.allocator),
            .path = path,
        };
        return &self.prev.?.card;
    }

    pub fn deinit(self: *CardIterator) !void {
        // Saving previous card.
        if (self.prev) |prev| {
            const file = try fs.cwd().openFile(prev.path, .{ .mode = .write_only });
            defer file.close();

            const string = try prev.card.toString(self.allocator);
            defer self.allocator.free(string);

            try file.writeAll(string);
        }

        if (self.prev) |*prev| {
            self.allocator.free(prev.path);
            try prev.card.deinit();
        }

        self.prev = null;
    }
};
