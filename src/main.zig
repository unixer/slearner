const std = @import("std");
const mem = std.mem;
const ray = @cImport(@cInclude("raylib.h"));
const Allocator = mem.Allocator;

const Deck = @import("deck.zig").Deck;
const CardIterator = @import("deck.zig").CardIterator;
const Result = @import("card.zig").Result;
const Card = @import("card.zig").Card;

const PickingDeck = struct {
    allocator: Allocator,
    dir: std.fs.Dir,
    iterator: std.fs.Dir.Iterator,
    current_entry: usize,
    entries: [][:0]const u8,

    pub fn open(allocator: Allocator, dir: std.fs.Dir) !PickingDeck {
        var iterator = dir.iterate();
        var arr = std.ArrayList([:0]const u8).init(allocator);

        while (try iterator.next()) |entry| {
            if (entry.kind == .directory and entry.name[0] != '.') {
                try arr.append(try allocator.dupeZ(u8, entry.name));
            }
        }

        try arr.append(try allocator.dupeZ(u8, ".."));

        return PickingDeck{
            .allocator = allocator,
            .dir = dir,
            .iterator = iterator,
            .current_entry = 0,
            .entries = try arr.toOwnedSlice(),
        };
    }

    pub fn cd(self: *PickingDeck) !void {
        self.* = try PickingDeck.open(self.allocator, try self.dir.openDir(self.entries[self.current_entry], .{ .iterate = true }));
    }

    fn clearEntries(self: *PickingDeck) void {
        for (self.entries) |entry| {
            self.allocator.free(entry);
        }
        self.entries = {};
    }
};

const Studying = struct {
    deck: *Deck,
    card: *Card,
    show_answer: bool,

    pub fn new(deck: *Deck) !?Studying {
        if (try deck.due()) |card| {
            return Studying{
                .deck = deck,
                .card = card,
                .show_answer = false,
            };
        } else return null;
    }

    /// Returns a State.studying with next due card or state.finished.
    pub fn next(self: *Studying) !State {
        var result: State = undefined;
        if (try Studying.new(self.deck)) |studying| {
            result = State{ .studying = studying };
        } else {
            try self.deinit();
            result = State{ .finished = {} };
        }
        return result;
    }

    pub fn deinit(self: *Studying) !void {
        try self.deck.deinit();
    }
};

const State = union(enum) {
    picking_deck: PickingDeck,
    studying: Studying,
    finished: void,
};

var font: ray.Font = undefined;

pub fn main() !void {
    var alloc = std.heap.GeneralPurposeAllocator(.{}){};
    const allocator = alloc.allocator();

    var state: State = State{ .picking_deck = try PickingDeck.open(allocator, try std.fs.cwd().openDir(".", .{ .iterate = true })) };

    const screenWidth = 1024;
    const screenHeight = 512;
    const borders = 100;
    const font_size = 20;
    const text_color = ray.WHITE;

    ray.InitWindow(screenWidth, screenHeight, "Slearner");
    defer ray.CloseWindow();

    ray.SetWindowState(ray.FLAG_WINDOW_RESIZABLE);
    ray.SetTargetFPS(10);

    {
        var codepoints: [512]c_int = [1]c_int{0} ** 512;
        var i: c_int = 0;
        while (i < 95) : (i += 1) codepoints[@intCast(i)] = 32 + i;
        i = 0;
        while (i < 255) : (i += 1) codepoints[@intCast(96 + i)] = 0x400 + i;

        const file = @embedFile("./assets/Merriweather-Regular.ttf");
        font = ray.LoadFontFromMemory(".ttf", file, file.len, font_size, &codepoints, codepoints.len);
    }

    while (!ray.WindowShouldClose()) {
        switch (state) {
            State.picking_deck => {
                const pressed_key = ray.GetKeyPressed();
                switch (pressed_key) {
                    ray.KEY_UP => {
                        state.picking_deck.current_entry -%= 1;
                        state.picking_deck.current_entry %= @intCast(state.picking_deck.entries.len);
                    },
                    ray.KEY_DOWN => {
                        state.picking_deck.current_entry +%= 1;
                        state.picking_deck.current_entry %= @intCast(state.picking_deck.entries.len);
                    },
                    ray.KEY_ENTER => try state.picking_deck.cd(),
                    ray.KEY_SPACE => {
                        try state.picking_deck.cd();
                        var deck = try Deck.load(try state.picking_deck.dir.realpathAlloc(allocator, "."), allocator);
                        if (try Studying.new(&deck)) |x| state = State{ .studying = x } else state = State{ .finished = {} };
                    },
                    else => {},
                }
            },
            State.studying => |*studying| {
                const pressed_key = ray.GetKeyPressed();
                if (!studying.show_answer and pressed_key == ray.KEY_SPACE) {
                    studying.*.show_answer = true;
                } else if (studying.show_answer) {
                    var result: ?Result = null;
                    switch (pressed_key) {
                        ray.KEY_ONE => result = .Bad,
                        ray.KEY_TWO => result = .Ok,
                        ray.KEY_THREE => result = .Good,
                        else => {},
                    }

                    if (result) |the_result| {
                        studying.card.review(the_result);
                        state = try studying.*.next();
                    }
                }
            },
            State.finished => {},
        }

        // Rendering
        ray.BeginDrawing();
        defer ray.EndDrawing();

        ray.ClearBackground(ray.DARKGRAY);
        switch (state) {
            State.picking_deck => {
                const height = ray.GetRenderHeight();
                var starting_entry: usize = 0;

                var fits: isize = @intCast(state.picking_deck.entries.len);
                for (0..state.picking_deck.entries.len) |i| {
                    if (borders + ((font_size + font_size / 2) * i) > height - borders - font_size) {
                        fits = @intCast(i);
                        break;
                    }
                }

                const before_the_end: isize = fits - ias(isize, state.picking_deck.current_entry);
                if (before_the_end < 1) {
                    starting_entry = @intCast(@abs(before_the_end));
                    starting_entry +|= 1;
                }

                for (starting_entry..starting_entry + ias(usize, fits)) |i| {
                    const entry = state.picking_deck.entries[i];

                    const i_visual: c_int = @intCast(i - starting_entry); // considering the starting opsition when drawing on the screen

                    const y = borders + i_visual * (font_size + font_size / 2);
                    drawText(entry, borders + (font_size * 2), y, font_size, text_color);
                    if (i == state.picking_deck.current_entry)
                        drawText("->", borders, y, font_size, text_color);
                }
                drawText("[up/down] - navigate    [enter] - enter directory    [space] - pick directory", borders, height - borders, font_size, text_color);
            },
            State.studying => |studying| {
                const height = ray.GetRenderHeight();

                drawText(studying.card.front.ptr, borders, borders, font_size, text_color);
                if (studying.show_answer) {
                    drawText(studying.card.back.ptr, borders, @divTrunc(height, 2) + borders, font_size, text_color);
                    drawText("[1] - bad    [2] - ok    [3] - good", borders, height - borders, font_size, text_color);
                } else {
                    drawText("[space] - show answer", borders, height - borders, font_size, text_color);
                }
            },
            State.finished => {
                drawText("Nothing to study.\n\nTIP: Add new cards by creating text files!", borders, borders, font_size, text_color);
            },
        }
    }
}

fn drawText(text: [*c]const u8, posX: c_int, posY: c_int, fontSize: c_int, color: ray.Color) void {
    ray.DrawTextEx(font, text, ray.Vector2{ .x = @floatFromInt(posX), .y = @floatFromInt(posY) }, @floatFromInt(fontSize), @as(f32, @floatFromInt(fontSize)) / 10.0, color);
}

/// Difference
fn dif(a: anytype, b: @TypeOf(a)) @TypeOf(a) {
    if (a > b) return a - b else return b - a;
}

fn ias(comptime T: type, x: anytype) T {
    return @as(T, @intCast(x));
}
