const std = @import("std");
const expect = std.testing.expect;
const mem = std.mem;
const meta = std.meta;
const Allocator = std.mem.Allocator;

const Date = @import("date.zig").Date;

pub const Result = enum {
    Bad,
    Ok,
    Good,
};

pub const Card = struct {
    front: [:0]const u8,
    back: [:0]const u8,
    due: Date,
    interval: u16,
    allocator: Allocator,

    pub fn review(self: *Card, result: Result) void {
        if (self.interval < 1) self.interval = 1;
        switch (result) {
            .Bad => {
                if (self.interval > 1)
                    self.interval /= 2;
            },
            .Ok => self.interval += self.interval / 4,
            .Good => self.interval *= 2,
        }
        self.due = Date.current().add(self.interval);
    }

    /// You are supposed to free the slice.
    pub fn toString(self: Card, allocator: Allocator) ![]const u8 {
        var result = std.ArrayList(u8).init(allocator);

        try result.ensureUnusedCapacity(self.front.len + self.back.len +
            8 + // Separators between values
            Date.maxDateStringLen + // Date
            5 // Interval
        );

        try std.fmt.format(
            result.writer(),
            "{s}\n\n{}\n\n{s}\n\n{s}",
            .{ try self.due.toString(allocator), self.interval, self.front, self.back },
        );

        return try result.toOwnedSlice();
    }

    /// Return null if failed to convert into card.
    /// It's safe to free the string.
    /// You need to call deinit to avoid a memory leak.
    pub fn fromString(string: []const u8, allocator: Allocator) !Card {
        const parseUnsigned = std.fmt.parseUnsigned;

        var result: Card = undefined;
        result.allocator = allocator;

        var iter = std.mem.splitSequence(u8, string, "\n\n");

        result.due = try Date.fromString(iter.next() orelse return error.@"Date not found");

        result.interval = try parseUnsigned(u16, iter.next() orelse return error.@"Interval not found", 10);

        result.front = try allocator.dupeZ(u8, iter.next() orelse return error.@"Front not found");
        result.back = try allocator.dupeZ(u8, iter.next() orelse return error.@"Back not found");

        return result;
    }

    pub fn eql(self: Card, other: Card) bool {
        return mem.eql(u8, self.front, other.front) and
            mem.eql(u8, self.back, other.back) and
            meta.eql(self.due, other.due) and
            self.interval == other.interval;
    }

    pub fn deinit(self: *Card) !void {
        self.allocator.free(self.front);
        self.allocator.free(self.back);
        self.* = undefined;
    }
};

test "ToString() readability" {
    const card = Card{
        .front = "Front.",
        .back = "Back.",
        .due = Date{ .day = 15, .month = 10, .year = 2023 },
        .interval = 1,
    };

    const expected_string =
        \\15.10.2023
        \\
        \\1
        \\
        \\Front.
        \\
        \\Back.
    ;

    const generated_string = try card.toString(std.testing.allocator);
    defer std.testing.allocator.free(generated_string);

    try expect(mem.eql(u8, generated_string, expected_string));
}

test "To/from String" {
    var allocator = std.testing.allocator_instance.allocator();

    const initial_card =
        Card{
        .front = "It's front",
        .back = "It's back",
        .due = Date{ .day = 8, .month = 10, .year = 2023 },
        .interval = 10,
    };

    const string = try initial_card.toString(allocator);
    defer allocator.free(string);

    const card_from_string = try Card.fromString(string);

    try expect(initial_card.eql(card_from_string));
}
