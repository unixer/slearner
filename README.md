# Slearner
## Flashcards in text files!
#### Building
You need the latest version of [zig](https://ziglang.org/download/)
And the [raylib library](https://github.com/raysan5/raylib/wiki) 
```sh
  zig build -Doptimize=ReleaseSafe
```
*Find the executable in zig-out/bin*
#### Status
Developing slowly.
PRs are welcome! (Look at issues with good first issue label)
